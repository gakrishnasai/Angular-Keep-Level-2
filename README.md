# Keep Assignment - Level 2

**Aim**: The objective of this level in the assignment is to cover the following areas.

- Component Design
- Component Interaction
- Authentication & Security
- Routing & Guards
- Directives
- Testing

**Description:**

**The Keep application demands following things to be covered:**
- Authentication: Authentication using Login View as discussed during the live sessions. The authentication api is already been created and can be cloned from [Authentication Server](https://gitlab-cts.stackroute.in/Ravi.Chandran/Authentication-Server). (Authentication, Routing & Guards)
- Notes View: A collection of notes.
- List View: A collection of notes but classified into three lists
	- Not Started
	- Started
	- Completed
	***Hint***: The notes may need an additional property to classify them into lists. 
- Edit Note View: Both the NotesView and the ListView should be able to edit notes and should be able to update the list and notes view. (Component Design & Component Interactions)
- Note Taker Component: Both the NotesView and the ListView should have a common NoteTakerComponent present in the DashboardComponent. (Named Router Outlets, Sharing Data Using Services)
- Unit Test Cases for all the Components.

